import aiohttp
import aiohttp.web
import os
import logging

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)


async def handle_js(request):
    script_path = os.path.join(os.path.dirname(__file__), 'script_v6.js')
    logger.info(f"Received request for {request.path}")

    if os.path.exists(script_path):
        logger.info(f"Serving {script_path}")
        return aiohttp.web.FileResponse(script_path)
    else:
        logger.error(f"{script_path} not found")
        return aiohttp.web.Response(status=404, text="JavaScript file not found")


async def init_app():
    app = aiohttp.web.Application()
    app.router.add_get('/s3/abt-challenge/script_v5.js', handle_js)
    return app


def main():
    app = init_app()
    logger.info("Starting server on http://0.0.0.0:8080")
    aiohttp.web.run_app(app, host='0.0.0.0', port=8080)


if __name__ == '__main__':
    main()
